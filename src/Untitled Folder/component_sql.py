from sqlite_base import Base
import pandas as pd


class SQL(Base):
    
    def __init__(self, db_name):
        super().__init__(db_name)
        
    
    
    def add_user_movie_data(self, user_id, movie_id, score):
        
        query = "INSERT INTO ratings (userId, movieId, rating, timestamp) VALUES ({}, {}, {}, 0)".format(user_id, movie_id, score)
        self.review_data(query)
       
        
       
    def get_one_user_data(self, user_id):
        
        query = "SELECT userId, movieId, rating FROM ratings WHERE userId = {}".format(user_id)
        datafrombd, namesfrombd = self.get_data(query)
        df = pd.DataFrame(datafrombd, columns = namesfrombd)
        return df
    
    
    
    def update_user_movie_data(self, user_id, movie_id, new_score):
        
        query = "UPDATE ratings SET rating = {} WHERE userId = {} AND movieId = {}".format(new_score, user_id, movie_id)
        self.review_data(query)
        
    
    
    def get_film_names(self, movie_ids):

        query = "SELECT title FROM movies WHERE movieId IN {}".format(movie_ids)
        datafrombd, namesfrombd = self.get_data(query)
        df = pd.DataFrame(datafrombd, columns = namesfrombd)
        return df
        
        
"""   
получить таблицу с названиями фильмов, которые смотрел первый пользователь
получить id фильмов для первого пользователя, преобразовать их в tuple, 
отправить в запрос get_film_names()  ЗАПРОС НУЖНО ДОПИСАТЬ !!

upd: реализована с помощью multiprocessing






получить таблицу с названиями фильмов, у которых оценка == 1
получить id фильмов с оценкой '1' преобразовать их в tuple, 
отправить в запрос get_film_names()  ЗАПРОС НУЖНО ДОПИСАТЬ !!   

upd: реализовать для оценок 1,2,3 с помощью multiprocessing




получить список жанров фильмов, у которых оценка равна 5 для пользователя 102
получить id фильмов с оценкой '5' для 102 преобразовать их в tuple, 
отправить в запрос get_film_genres()  ЗАПРОС НУЖНО НАПИСАТЬ по примеру get_film_names!!   

upd: записать таблицы для пользователей 102, 103, 104, 105 с помощью asyncio






пример получения списка id для запроса
df = db.get_one_user_data(520)
tuple(df['movieId'].values)
"""
        
        