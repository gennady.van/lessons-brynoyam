# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 21:36:52 2021

@author: Admin
"""

from multiprocessing import Process
from threading import Thread
from component_sql import SQL
from time import time







def get_users_film_names(user_id):
    
    db = SQL('movie_data.db')
    df = db.get_one_user_data(user_id)
    ids = tuple(df['movieId'].values)
    df_film_names = db.get_film_names(ids)
    
    df_film_names.to_csv('{}.csv'.format(user_id), index = None)








if __name__ == "__main__":
    
    
    a = time()
    process1 = Process(target=get_users_film_names, args=(1,))
    process2 = Process(target=get_users_film_names, args=(2,))
    process1.start()
    process2.start()
    print(time() - a)
    
    
    
    
    
    """
    a = time()
    get_users_film_names(1)
    get_users_film_names(2)
    print(time() - a)
    0.03992128372192383
    """
    
    