import sqlite3

class Base:
    
    
    def __init__(self, db_name):
        
        self.__db_name = db_name
        
    
    
    def __create_connection(self):
        self.__connection = sqlite3.connect(self.__db_name)
        
    
    
    def __close_connection(self):
        self.__connection.close()
        
        
    
    def get_data(self, query):
        self.__create_connection()
        cursor = self.__connection.cursor()
        result = cursor.execute(query)
        datafrombd = result.fetchall()
        namesfrombd = [i[0] for i in cursor.description]
        self.__close_connection()
        
        return datafrombd, namesfrombd
    
    
    
    def review_data(self, query):
        self.__create_connection()
        cursor = self.__connection.cursor()
        cursor.execute(query)
        self.__connection.commit()
        self.__close_connection()