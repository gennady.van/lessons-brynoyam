# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 20:13:05 2021

@author: Admin
"""

import asyncio
from multiprocessing import Process
from threading import Thread
import numpy as np
import pandas as pd
import time

array = np.random.random_sample((10000, 3))
df = pd.DataFrame(array, columns = ['A', 'B', 'C'])
df.to_csv('array.csv', index = None)

data = pd.read_csv('array.csv')

A = data['A'].values
B = data['B'].values
C = data['C'].values



def mean(array, name_array):
    
    S = 0
    L = 0
    for i in array:
        S += i
        L += 1
    M = S/L
    with open('result.txt', 'a') as f:
        f.write('mean {} = {} \n'.format(M, name_array))
    
        
    
    
    
    
async def a_mean(array_of_arrays, names_of_arrays):
    S = 0
    L = 0
    for a,n in zip(array_of_arrays, names_of_arrays):
        for i in array:
            S += i
            L += 1
        M = S/L
        with open('result.txt', 'a') as f:
            f.write('mean {} = {} \n'.format(a,n))
    
    
        
if __name__ == "__main__":
    
    
    
    array_of_arrays = [A, B, C]
    names_of_arrays = ['A', 'B', 'C']
    t0 = time.time()
    for a,n in zip(array_of_arrays, names_of_arrays):
        mean(a,n)
    t1= time.time() - t0 
    
   
    
   
    
    thread1 = Thread(target=mean, args=(A,'A'))
    thread2 = Thread(target=mean, args=(B,'B'))
    thread3 = Thread(target=mean, args=(C,'C'))
    thread1.start()
    thread2.start()
    thread3.start()

    
    

    process1 = Process(target=mean, args=(A,'A'))
    process2 = Process(target=mean, args=(B,'B'))
    process3 = Process(target=mean, args=(C,'C'))
    t0  = time.time()
    process1.start()
    process2.start()
    process3.start()
    t2 = time.time() - t0 
    
    
    
    array_of_arrays = [A, B, C]
    names_of_arrays = ['A', 'B', 'C']
    
    
    t0  = time.time()
    loop = asyncio.get_event_loop()
    futures = asyncio.gather(
        a_mean(array_of_arrays, names_of_arrays),
    )
    t3 = time.time() - t0 
    

"""
if __name__ == "__main__":
    
    #
    a = time()
    process1 = Process(target=get_users_film_names, args=(1,))
    process2 = Process(target=get_users_film_names, args=(2,))
    process1.start()
    process2.start()
    print(time() - a)
    """



"""


создать три одномерных массива размерностью 10к элементов, записать в файл data
считать файл data
посчитать среднее арифметическое без использования функций sum, len и т.д.
полученный результат записать в файл results

использовать для вычисления asyncio, Process, Thread







"""